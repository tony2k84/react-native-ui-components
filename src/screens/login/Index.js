import React from 'react';

import {
  StackNavigator,
} from 'react-navigation';

import LoginScreens from './Login';
import DarkLoginScreen2 from './ui/DarkLoginScreen2';
import LoginScreen3 from './ui/LoginScreen3';
import LoginScreen4 from './ui/LoginScreen4';
import LoginScreen5 from './ui/LoginScreen5';
import LoginScreen5a from './ui/LoginScreen5a';
import LoginScreen5b from './ui/LoginScreen5b';
import LoginScreen6 from './ui/LoginScreen6';
import LoginScreen7 from './ui/LoginScreen7';
import LoginScreen8 from './ui/LoginScreen8';

const LoginNavigator = StackNavigator(
	{
  		LoginScreens: { 
  			path: "/login",
  			screen: LoginScreens 
  		},

      DarkLoginScreen2: {
        path: "/login/darkloginscreen2", 
        screen: DarkLoginScreen2
      },
      LoginScreen3: {
        path: "/login/loginscreen3", 
        screen: LoginScreen3,
      },
      LoginScreen4: {
        path: "/login/loginscreen4", 
        screen: LoginScreen4,
      },
      LoginScreen5: {
        path: "/login/loginscreen5", 
        screen: LoginScreen5,
      },
      LoginScreen5a: {
        path: "/login/loginscreen5a", 
        screen: LoginScreen5a,
      },
      LoginScreen5b: {
        path: "/login/loginscreen5b", 
        screen: LoginScreen5b,
      },
      LoginScreen6: {
        path: "/login/loginscreen6", 
        screen: LoginScreen6,
      },
      LoginScreen7: {
        path: "/login/loginscreen7", 
        screen: LoginScreen7,
      },
      LoginScreen8: {
        path: "/login/LoginScreen8", 
        screen: LoginScreen8,
      },

	},
	{
		initialRouteName: 'LoginScreens',
		headerMode: 'none',
	});

export default LoginNavigator;