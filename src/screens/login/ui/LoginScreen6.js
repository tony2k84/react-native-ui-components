import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'



const theme = {
	DEFAULT: '#FEFEFE',
	PRIMARY: '#3389e8',
	SECONDARY: '#0075f7',
};

class LoginScreen6 extends Component {
  
  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      	<View style={styles.container}>
      		<View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>  
              <View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>
                <Image style={{flex: 1, resizeMode: 'cover'}} source={{ uri: 'https://cdn.pixabay.com/photo/2015/03/26/09/47/sky-690293_640.jpg' }}/>
              </View>
              <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: '#4392F1', opacity: 0.4}}/>
            </View>
            <View style={styles.content}>
            	<View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
					<Feather name='cloud' style={styles.logo}/>
					<Text style={styles.title}>CLOUD</Text>
				</View>
			</View>
			<View style={styles.footer}>
				<View style={styles.inputWrapper}>
					<Feather name='user' style={styles.icon}/>
			      	<TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.DEFAULT}
				      placeholder='Email Address'
				      style={styles.input}
				      />
			    </View>
			    <View style={styles.seperator}/>
				<View style={styles.inputWrapper}>
					<Feather name='unlock' style={styles.icon}/>
			      	<TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.DEFAULT}
				      placeholder='Password'
				      style={styles.input}
				      />
				    <Text style={styles.disabledText}>Forgot Password?</Text>
			    </View>
			    <TouchableOpacity style={styles.button} onPress={() => this._back()}>
					<Text style={styles.buttonText}>Sign In</Text>
				</TouchableOpacity>
				<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 15}}>
					<View style={{flexDirection: 'row'}}>
						<Text style={styles.disabledText}>Don't have an account yet?</Text>
						<Text style={styles.linkText}>Sign Up</Text>
					</View>
				</View>
			</View>
      	</View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},
	content: {
		flex: 1,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignItems: 'center',
	},

	footer: {
		flexDirection: 'column',
	},

	logo: {
		color: theme.DEFAULT,
		fontSize: 100,
	},

	title: {
		color: theme.DEFAULT,
		fontSize: 24,
		fontFamily: 'QuattrocentoSansBold',
	},

	icon: {
		color: theme.DEFAULT,
		fontSize: 20,
	},

	seperator: {
		borderBottomWidth: 0.5,
		borderBottomColor: '#BBC7FF',
	},

	inputWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 15,
	    paddingVertical: 10,
	    backgroundColor: theme.SECONDARY,
	    opacity: 0.7,
	},

	input: {
		flex: 1,
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.DEFAULT,
	  	paddingVertical: 10,
	  	paddingHorizontal: 10,	  	
	},

	button: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	    paddingVertical: 15,
	    backgroundColor: theme.PRIMARY,
	},

	buttonText: {
		fontSize: 16,
		fontFamily: 'QuattrocentoSans',
		color: theme.DEFAULT,
	},

	disabledText: {
		fontSize: 13,
		fontFamily: 'QuattrocentoSans',
		color: '#D2DBE8',
		backgroundColor: 'transparent',
	},

	linkText: {
		fontSize: 16,
		paddingHorizontal: 5,
		fontFamily: 'QuattrocentoSans',
		color: theme.DEFAULT,
		backgroundColor: 'transparent',
	},


});


export default LoginScreen6;