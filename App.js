import React from 'react';
import IndexNavigator from './src/screens/Index';

//disable yellow warning on expo 
console.disableYellowBox = true;

export default class App extends React.Component {

  state = {
    isReady: false,
  };

  async componentDidMount() {
    
    await Expo.Font.loadAsync({
      //'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'QuattrocentoSans': require('./assets/fonts/QuattrocentoSans-Regular.ttf'),
      'QuattrocentoSansBold': require('./assets/fonts/QuattrocentoSans-Bold.ttf'),
    });

    console.log('Loaded')

    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }
    return <IndexNavigator />;
  }
}