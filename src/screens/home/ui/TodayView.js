'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#DEE8D5',
	PRIMARY: '#212121',
	SECONDARY: '#AAACB3',
	HIGHLIGHT: '#426B69',
};


class TodayView extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }
  
  render() {
    return (
    	<View style={styles.container}>
	    	<View style={styles.header}>
	    		<TouchableOpacity onPress={() => this._back()}>
	    			<Feather name='chevron-left' style={styles.icon}/>
	    		</TouchableOpacity>
	    		<View style={styles.column}>
	    			<Text style={styles.heading}>Today</Text>
	    			<Text style={styles.subHeading}>22 Feb</Text>
	    		</View>	    		
	    		<View/>
	    	</View>
	    	<View style={styles.content}>
	    		<View style={[styles.column, {paddingVertical: 10}]}>
	    			<View style={styles.column}>
		    			<Text style={styles.text}>Workout</Text>
		    			<Text style={styles.subText}>6:00 AM</Text>
	    			</View>
	    		</View>

	    		<View style={[styles.column, {paddingVertical: 10}]}>
	    			<View style={styles.column}>
		    			<Text style={styles.text}>Daily Standup</Text>
		    			<Text style={styles.subText}>9:00 AM</Text>
	    			</View>
	    		</View>

	    		<View style={[styles.column, {paddingVertical: 10}]}>
	    			<View style={styles.column}>
		    			<Text style={styles.text}>Coffee with Wife</Text>
		    			<Text style={styles.subText}>11:30 AM</Text>
	    			</View>
	    		</View>

	    	</View>
	    	<View style={styles.footer}>
	    		<View/>
	    		<View style={styles.column}>
	    			<Feather name='cloud-lightning' style={styles.mediumicon}/>
	    			<View style={{paddingVertical: 2}}/> 
	    			<Text style={styles.text}>18&deg;</Text>
    			</View>
    			<View/>
	    	</View>
    	</View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10,
	},

	column: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},

	content: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},

	heading: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 20,
	  	color: theme.PRIMARY,
	},

	subHeading: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 13,
	  	color: theme.SECONDARY,
	},

	text: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 15,
	  	color: theme.PRIMARY,
	},

	subText: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 13,
	  	color: theme.SECONDARY,
	},

	icon: {
		fontSize: 24,
		color: theme.HIGHLIGHT,
	},

	mediumicon: {
		fontSize: 30,
		color: theme.HIGHLIGHT,
	},

	footer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10,
	},

});


export default TodayView;