import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import {
  StackNavigator,
} from 'react-navigation';

import HomeNavigator from './home/Index';
import ListNavigator from './list/Index';
import LoginNavigator from './login/Index';

class IndexScreen extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = {
	  	
	  	designList: [
	  		{
		  		key: 1,
	            title: 'General',
	            navigate: 'GeneralScreens'
        	},
        	{
		  		key: 2,
	            title: 'List',
	            navigate: 'ListScreens'
        	},
        	{
		  		key: 3,
	            title: 'Login',
	            navigate: 'LoginScreens'
        	},
	  	]
	  };

	  this.navigateTo = this.navigateTo.bind(this);

	}

	navigateTo(to) {
	    console.log('-->', to);
	    this.props.navigation.navigate(to)
  	}
  
  	render() {
  		var designList = this.state.designList;
	    return (
		    <View style={styles.container}>
		    	<View style={styles.header}>            
		            <Text style={styles.listTitle}>What UI you are looking for today?</Text>
		        </View>
		      	<StatusBar hidden/>
		      	
		      	{designList.map((item) => {          
		            return (
		            	<TouchableOpacity style={styles.listItem} key={item.key} onPress={() => this.navigateTo(item.navigate)}>
		            		<Text style={styles.itemText}>{item.title}</Text>
		            	</TouchableOpacity>		               
		            )
		          })
		        }
		    </View>
		);
  	}
}

const IndexNavigator = StackNavigator(
	{
		IndexScreen: { 
  			path: "/",
  			screen: IndexScreen 
  		},
  		GeneralScreens: { 
  			path: "/home",
  			screen: HomeNavigator 
  		},  		
  		LoginScreens: {
  			path: "/login",
  			screen: LoginNavigator 
  		},
  		ListScreens: {
  			path: "/list",
  			screen: ListNavigator 
  		}
	},
	{
		initialRouteName: 'IndexScreen',
		headerMode: 'none',
	});

const styles = StyleSheet.create({
	container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#FFFFFF',
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 10,
    marginBottom: 30,
  },

  icon: {
    fontSize: 40,
    color: '#6A6262',
  },

  list: {
    padding: 10,
  },

  listTitle: {
    fontFamily: 'QuattrocentoSansBold',
    fontWeight: 'bold',
    fontSize: 28,
    color: '#6A6262',
  },

  listItem: {
    paddingVertical: 20,
  },

  itemText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 20,
    color: '#6A6262',   
  },

  itemSubText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 16,
    color: '#9C9990', 
  }


});


export default IndexNavigator;