//https://in.pinterest.com/pin/292452569544127646/

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#313E50',
	SECONDARY: '#2967cc',
	DISABLED: '#878C8F',
};

class LoginScreen5 extends Component {

  _next(){
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'LoginScreen5a' }));
  }

  _back(){
    this.props.navigation.dispatch(NavigationActions.back());
  }

  render() {
    return (
      <View style={styles.container}>
      	<View style={styles.content}>
      		<Text style={styles.heading}>
      			Life is really simple, but we insist on making it complicated.
      		</Text>
      		<Text style={styles.subText}>Confucius</Text>
      	</View>	
      	<View style={styles.footer}>
      		<TouchableOpacity onPress={() => this._next()}>
      			<Text style={styles.linkText}>Sign In</Text>
      		</TouchableOpacity>
      		<TouchableOpacity onPress={() => this._back()}>
      			<Text style={styles.linkText}>    </Text>
      		</TouchableOpacity>
      	</View>	
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
	},

	content: {
		flex: 1,
	},

	heading: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
  		fontSize: 36,
	  	color: theme.PRIMARY,
	  	marginTop: 100,
	  	marginBottom: 10,
	},

	subText: {
		fontFamily: 'QuattrocentoSans',
		fontSize: 17,
	  	color: theme.DISABLED,
	},

	linkText: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		fontSize: 18,
	  	color: theme.SECONDARY,
	},

	footer: {
		paddingVertical: 5,
		flexDirection: 'row',
		justifyContent: 'space-between',
	}
});


export default LoginScreen5;