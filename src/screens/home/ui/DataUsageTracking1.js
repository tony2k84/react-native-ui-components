import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#1a1423',
	PRIMARY: '#ffffff',
	SECONDARY: '#c8d3d5',
	HIGHLIGHT: '#0cca4a',
};

class DataUsageTracking1 extends Component {
  
  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <View style={styles.container}>
      	<View style={styles.header}>
	    	<View style={styles.titleWrapper}><Text style={styles.title}>DATA USAGE</Text></View>
	    </View>
	    <View style={styles.content}>
	    	<View style={styles.progressBarWrapper}>
	    		<View style={styles.progressBar}>	    			
	    		</View>
	    		<View style={styles.progressBarValueWrapper}>
	    			<Text style={styles.progressBarValue}>20%</Text>
	    		</View>	    		
	    	</View>
	    	<View style={{paddingTop: 10}}/>
	    	<Text style={styles.text}>Next refill in 20 days</Text>
	    </View>
	    <View style={styles.footer}>
	    	<TouchableOpacity style={styles.button} onPress={() => this._back()}>
	    		<Text style={styles.buttonText}>Check your daily usage</Text>
	    	</TouchableOpacity>
	    </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 40,
	},
	content: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	footer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		padding: 30,
	},

	titleWrapper: {
		padding: 2,
		borderBottomWidth: 2,
		borderBottomColor: theme.SECONDARY,
	},

	title: {
		color: theme.PRIMARY,
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		fontSize: 24,		
	},

	progressBarWrapper: {
		width: '70%',
		borderWidth: 3,
		borderColor: theme.HIGHLIGHT,
		borderRadius: 100,
		height: 50,
		padding: 0,
		margin: 0,
	},

	progressBar: {
		position: 'absolute',
		width: 80,
		height: '100%',
		backgroundColor: theme.HIGHLIGHT,
		borderBottomLeftRadius: 20,
		borderTopLeftRadius: 20,
	},

	progressBarValueWrapper: {
		height: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	progressBarValue: {
		color: theme.PRIMARY,
		fontFamily: 'QuattrocentoSans',
		fontSize: 18,
	},

	button: {
		borderWidth: 2,
		borderColor: theme.SECONDARY,
		borderRadius: 100,
		paddingHorizontal: 25,
		paddingVertical: 10
	},

	buttonText: {
		color: theme.PRIMARY,
		fontFamily: 'QuattrocentoSans',
		fontSize: 18,
	},

	text: {
		color: theme.SECONDARY,
		fontFamily: 'QuattrocentoSans',
		fontSize: 17,
	}

});


export default DataUsageTracking1;