'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#313E50',
	SECONDARY: '#36CC22',
};

class ContactUsScreen1 extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }
  
  render() {
    return (
    	<View style={styles.container}>
      		<View style={{padding: 10, backgroundColor: 'transparent'}}>
            	<TouchableOpacity onPress={() => this._back()}>
            		<Feather name='arrow-left' color={theme.SECONDARY} style={{fontSize: 30}}/>
            	</TouchableOpacity>
            </View>
            <View style={styles.content}></View>
			<View style={{padding: 20}}>
				<Text style={styles.heading}>Get in touch if you need help with a project.</Text>
				<Text style={styles.text}>London</Text>
				<Text style={styles.text}>73 Caerfai Bay Road</Text>
				<Text style={styles.text}>TETBURY GL8 0SZ</Text>
				<Text style={styles.buttonText}>Get directions</Text>
			</View>
			<View style={{marginTop: 10, padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
				<View style={{flexDirection: 'column'}}>
					<Text style={styles.text}>hello@getyourjobdone.com</Text>
					<Text style={styles.text}>020 1233 3232</Text>
				</View>
				<View style={{flexDirection: 'row'}}>
					<View style={styles.iconWrapper}>
						<Feather name='mail' style={styles.icon}/>
					</View>
					<View style={styles.iconWrapper}>
						<Feather name='phone' style={styles.icon}/>
					</View>
				</View>
			</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},
	content: {
		flex: 1,
	},

	heading: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
  		fontSize: 36,
	  	color: theme.PRIMARY,
	  	marginBottom: 20,
	},

	text: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.PRIMARY,	
	  	marginBottom: 5,
	},

	buttonText: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
  		fontSize: 17,
	  	color: theme.PRIMARY,
	  	marginBottom: 5,
	},

	iconWrapper: {
		borderRadius: 100,
		padding: 10,
		marginHorizontal: 5,
		backgroundColor: theme.SECONDARY,
	},

	icon: {
		fontSize: 20,
		color: theme.DEFAULT,
	},


});


export default ContactUsScreen1;