import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  ImageBackground,
  ScrollView
} from 'react-native';

import { NavigationActions } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class DarkLoginScreen2 extends Component {

  constructor(props) {
    super(props);

    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){
    console.log('Navigating back..')
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {

    return (
      <ImageBackground
        source={require('../../../../assets/login-ui-screen2.jpg')}
        style={styles.container}>

        <View style={styles.overlayContainer}>
          <ScrollView contentContainerStyle={styles.content}>           

          	<Image source={require('../../../../assets/login-ui-screen2-icon.png')}
                   style={styles.appIcon}/>

            <View style={styles.form}>

              <View style={styles.formItem}>
                <MaterialIcons active name='mail-outline' style={styles.formIcon} />
                <TextInput placeholder='E-mail address' placeholderTextColor="#ffffff" style={styles.formInput}/>
              </View>

              <View regular style={styles.formItem}>
                <MaterialIcons active name='lock-outline' style={styles.formIcon} />
                <TextInput placeholder='********' placeholderTextColor="#ffffff" style={styles.formInput}/>
              </View>

              <TouchableOpacity
                 style={styles.button}
                 onPress={this._onPressButton}>
                <Text style={styles.buttonText}> Sign in </Text>
              </TouchableOpacity>


              <TouchableOpacity
                 style={styles.transparentButton}
                 onPress={this._onPressButton}>
                <Text style={styles.normalText}> Forgot password? </Text>
              </TouchableOpacity>

             
            </View>


          </ScrollView>
          <View style={styles.footer}>
            <View style={styles.signUpView}>
              <TouchableOpacity
                 style={styles.transparentButton}
                 onPress={this._onPressButton}>
                <Text style={styles.normalText}> New here? Sign Up </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles =  StyleSheet.create({
    container: {
      flex: 1,
    },

    overlayContainer: {
      flex: 1,
    },

    content: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',

      backgroundColor: 'rgba(0,0,0,.4)',
      paddingHorizontal: 40,
      paddingVertical: 10,
    },

    appIcon: {
      width: 64,
      height: 64,
    },

    form: {
      marginTop: 80,
      width: '100%',
    },

    formItem: {      
      backgroundColor: '#000000',
      opacity: 0.6,
      paddingHorizontal: 10,
      borderRadius: 5,
      borderTopColor: '#000000',
      borderRightColor: '#000000',
      borderBottomColor: '#000000',
      borderLeftColor: '#000000',
      marginBottom: 10,
    },

    formIcon: {
      color: '#e3c61f',
      fontSize: 32,
      marginRight: 2,
    },

    formInput: {
      marginHorizontal: 5,
      color: '#FFFFFF',
      fontFamily: 'QuattrocentoSans',
      width: 300,
      fontSize: 18,
    },

    button: {
      marginTop: 20,
      alignItems: 'center',
      backgroundColor: '#e3c61f',
      paddingVertical: 12,
      marginHorizontal: 30,
      borderRadius: 100,
    },

    transparentButton: {
      marginTop: 20,
      alignItems: 'center',
      opacity: 0.9,
    },

    buttonText: {
      color: '#ffffff',
      fontSize: 20,
      fontFamily: 'QuattrocentoSans',
      fontWeight: 'bold',
    },

    normalText: {
      color: '#d5d5d5',
      fontSize: 16,
      fontFamily: 'QuattrocentoSans',
    },

    footer: {
      backgroundColor: 'rgba(0,0,0,.4)',
      paddingHorizontal: 40,
      paddingVertical: 20,
    },

    signUpView: {
      borderTopColor: '#959595',
      borderTopWidth: 1,
    }

})
