'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#2A628F',
	SECONDARY: '#362C28',
  DISABLED: '#A39F9D',
};

class TaskSummaryView extends Component {
  
  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={[styles.subText, styles.default]}>Since the beginning</Text>
          <TouchableOpacity onPress={() => this._back()}>
            <Feather name='arrow-up' style={styles.icon}/>
          </TouchableOpacity>
        </View>
        <View style={styles.content}>
          <View style={styles.top}>
            <Text style={[styles.heading, styles.default]}>70</Text>
            <Text style={[styles.text, styles.default]}>Tasks</Text>
            <Text style={[styles.text, styles.default]}>Completed</Text>
           <Text style={[{paddingTop: 10}, styles.subText, styles.default]}>You mostly complete your tasks on Wednesday!</Text>
          </View>
          <View style={styles.bottom}>
            <Text style={[styles.heading, styles.disabled]}>30</Text>
            <Text style={[styles.text, styles.disabled]}>Tasks</Text>
            <Text style={[styles.text, styles.disabled]}>Completed</Text>
            <Text style={[{paddingTop: 10}, styles.subText, styles.disabled]}>Try starting tasks on Monday's!</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    padding: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.PRIMARY,
  },
  content: {
    flex: 1,
  },
  top: {
    flex: 7,
    flexDirection: 'column',
    backgroundColor: theme.PRIMARY,
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 20,
  },
  bottom: {
    flex: 3,
    flexDirection: 'column',
    backgroundColor: theme.SECONDARY,
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: 20,
  },

  heading: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 60,
  },

  text: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 32,
  },

  subText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 18,
  },

  default: {
    color: theme.DEFAULT,
  },

  disabled: {
    color: theme.DISABLED,
  },

  icon: {
    fontSize: 22,
    color: theme.DEFAULT,
  }


});


export default TaskSummaryView;