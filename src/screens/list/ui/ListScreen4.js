import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation'

class ListScreen4 extends Component {

	constructor(props) {
	  super(props);

	  var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
	  var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	  var dates = [];
	  var today = new Date()

	  for(var day=0; day<10; day++){
	  	var date = new Date();
	  	date.setDate(date.getDate()+day)
	  	dates.push({
	  		key: day,
	  		day: date.getDate(),
	  		weekDay: days[date.getDay()],
	  		date: date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear()
	  	})
	  }
	
	  this.state = {
	  	dates: dates,
	  	selectedItem: dates[0],
	  	completedTasks: [
	  		{
	  			key: 1,
	  			title: 'Create An Awesome TO-DO List UI',
	  			by: '19:30'
	  		}
	  	],
	  	pendingTasks: [
	  		{
	  			key: 1,
	  			title: 'Learn Breathing For Breast Stroke',
	  			by: '21:30'
	  		}
	  	],
	  };

	  this.navigateTo = this.navigateTo.bind(this);
	  this.showTasks = this.showTasks.bind(this);

	}

	showTasks(item){
		console.log(item)
		this.setState({selectedItem: item})
	}

	navigateTo(to) {
		//console.log('-->', to);
	    //this.props.navigation.navigate(to)
	    this.props.navigation.dispatch(NavigationActions.back())
	}

	render() {
  		const {dates, selectedItem, completedTasks, pendingTasks} = this.state;
    	return (
      		<View style={styles.container}>
			    <View style={styles.leftNav}>
			    	<FlatList
				  	data={dates}
				  	extraData={this.state}
				  	renderItem={({item}) => 
					  			<TouchableOpacity 
					  				style={(item.key===selectedItem.key)?[styles.flatListItemSelected]: [styles.flatListItem]}
					  				onPress={() => this.showTasks(item)}>
				    				<Text style={(item.key===selectedItem.key)?[styles.fontBold, styles.semidark, styles.h1]:[styles.font, styles.semidark, styles.h1]}>
				    					{item.day}
				    				</Text>
				    				<Text style={[{paddingTop: 5,}, styles.font, styles.light, styles.h2]}>
				    					{item.weekDay}
				    				</Text>
								</TouchableOpacity>			    			
			    		}
					/>
			    </View>
			    <View style={styles.rightContent}>
			    	
			    	<View style={styles.header}>
			    		<MaterialIcons name="navigate-before" style={[styles.light, styles.h2Icon]} 
			    			onPress={() => this.navigateTo('back')} />
			    		<Text style={[styles.font, styles.h2, styles.light]}>{selectedItem.date}</Text>
			    		<MaterialIcons name="add" style={[styles.light, styles.h2Icon]} />
			    	</View>

			    	<View style={styles.content}>
			    		
			    		<FlatList style={styles.flatList}
						  	data={completedTasks}
						  	renderItem={({item}) => 
						  			<View style={styles.completedTaskWrapper}>
					    				<View style={{flexDirection: 'column'}}>
					    					<Text style={styles.completedTaskText}>
					    						{item.by}
					    					</Text>
					    					<Text style={styles.completedTaskText}>
					    						{item.title}
					    					</Text>
					    				</View>

					    				<MaterialIcons name="check-circle" style={styles.completedTaskIcon} />

					    			</View>
					    		}
							/>

						<View style={{paddingHorizontal: 10, }}>
							<View style={{borderBottomWidth: 1, borderBottomColor: '#212121'}}/>
						</View>

						<FlatList style={styles.flatList}
						  	data={pendingTasks}
						  	renderItem={({item}) => 
						  			<View style={styles.pendingTaskWrapper}>
					    				<View style={{flexDirection: 'column'}}>
					    					<Text style={styles.pendingTaskText}>
					    						{item.by}
					    					</Text>
					    					<Text style={styles.pendingTaskText}>
					    						{item.title}
					    					</Text>
					    				</View>

					    				<MaterialIcons name="radio-button-unchecked" style={styles.pendingTaskIcon} />

					    			</View>
					    		}
							/>

						<View style={{paddingHorizontal: 10, }}>
							<View style={{borderBottomWidth: 1, borderBottomColor: '#212121'}}/>
						</View>

			    	</View>
			    </View>
		    </View>
		);
  }
}

const styles = StyleSheet.create({
	
	container: {
		flex: 1,
		flexDirection: 'row',
	},

	rightContent: {
		flex: 1,
		backgroundColor: '#0f1829',
		
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: 10,
		paddingVertical: 20,
		borderBottomWidth: 1, borderBottomColor: '#212121'
	},

	content: {
		padding: 10,
		paddingTop: 20,
	},

	flatList: {
		paddingHorizontal: 10, 
		paddingVertical: 40,
	},

	completedTaskWrapper: {
		paddingLeft: 10,
		borderLeftWidth: 2, 
		borderLeftColor: '#0184f6', 
		flexDirection: 'row', 
		justifyContent: 'space-between'
	},

	completedTaskText: {
		textDecorationLine: 'line-through',
		fontFamily: 'QuattrocentoSans',
		color: '#333333',
		fontSize: 14,
	},

	completedTaskIcon: {
		fontSize: 16,
		color: '#0184f6',
	},

	pendingTaskWrapper: {
		paddingLeft: 10,
		borderLeftWidth: 2, 
		borderLeftColor: '#12d48d', 
		flexDirection: 'row', 
		justifyContent: 'space-between'
	},

	pendingTaskText: {
		fontFamily: 'QuattrocentoSans',
		color: '#c7c7c7',
		fontSize: 14,
	},

	pendingTaskIcon: {
		fontSize: 16,
		color: '#ffffff',
	},

	h1: {
		fontSize: 22,
	},

	h2: {
		fontSize: 18,
	},

	h2Icon: {
		fontSize: 24,
	},

	font: {
		fontFamily: 'QuattrocentoSans',
	},

	fontBold: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
	},

	flatListItem: {
		flexDirection: 'column', 
		justifyContent: 'center', 
		alignItems: 'center', 
		paddingHorizontal: 20,
		paddingVertical: 30, 
		backgroundColor: '#011a30',
	},
	flatListItemSelected: {
		flexDirection: 'column', 
		justifyContent: 'center', 
		alignItems: 'center', 
		paddingHorizontal: 20,
		paddingVertical: 30, 
		backgroundColor: '#0184f6',
	},

	light: {
		color: '#ffffff',
	},

	semidark: {
		color: '#c7c7c7',
	},


});


export default ListScreen4;