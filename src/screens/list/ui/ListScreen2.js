import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  TextInput,
  Image
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation'

class ListScreen2 extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        designList: [
          {
            key: 1,
            name: 'Tony Stark',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
          {
            key: 2,
            name: 'Captain America',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
          {
            key: 3,
            name: 'Winter Soldier',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
          {
            key: 4,
            name: 'Natasha Romonoff',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
          {
            key: 5,
            name: 'Thor',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
          {
            key: 6,
            name: 'Loki',
            contact: '+91-847347633',
            navigate: 'NOTHING',
          },
        ]
    };

    this.back = this.back.bind(this);

  }

  back() {
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
  	var designList = this.state.designList;

    return (
      <View style={styles.container}>
	      <View style={styles.header}>

	      	<View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10,}}>
	      		<TouchableOpacity onPress={this.back}>
	      			<MaterialIcons name="keyboard-backspace" style={styles.light, styles.h1Icon} />
	      		</TouchableOpacity>
	      		<Text style={[styles.fontBold, styles.dark, styles.h3]}>Designer</Text>
	      	</View>

	      	<View style={{flexDirection: 'column', padding: 10, marginTop: 10,}}>
	      		<Text style={[styles.fontBold, styles.dark, styles.h1]}>Your friends</Text>
	      		<Text style={[styles.font, styles.semidark, styles.normal]}>200 available</Text>
	      	</View>

	      	<View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10}}>
	      		<View style={styles.activeLink}>
	      			<Text style={[styles.fontBold, styles.dark, styles.h3]}>All friends</Text>
	      		</View>
	      		<View>
	      			<Text style={[styles.font, styles.semidark, styles.h3]}>Groups</Text>
	      		</View>
	      		<View>
	      			<Text style={[styles.font, styles.semidark, styles.h3]}>Favourites</Text>
	      		</View>
	      		<TouchableOpacity style={styles.button}>
	      			<MaterialIcons name="add" style={[styles.light, styles.h2Icon]} />
	      			<Text style={[styles.font, styles.normal, styles.light]}>ADD</Text>
	      		</TouchableOpacity>
	      	</View>

	      	<View style={{paddingHorizontal: 10,}}>
		      	<View style={{paddingHorizontal: 10}}>
	                <MaterialIcons active name='search' style={[styles.h15Icon, styles.semidark]} />
	                <TextInput 
	                	placeholder='Search by name or number' 
	                	placeholderTextColor="#b7b7b7" 
	                	style={[styles.font,styles.h3, styles.semidark]}/>
	            </View>
            </View>
	      </View>

	      <ScrollView style={styles.content}>
	      	{designList.map((item) => { 
	      		  
                  return (
                    <TouchableOpacity 
                      key={item.key} 
                      style={styles.listItem} 
                      onPress={() => this.navigateTo(item.navigate)}>
                      	<View style={styles.listThumbNailWrapper}>
                      	    <Image style={styles.listThumbNail} source={require('../../../../assets/friend1.png')}/>
                      	</View>    	
				      	<View style={styles.listBody}>
					      	<Text style={[styles.fontBold, styles.h2, styles.dark]}>{item.name}</Text>
					      	<Text style={[styles.font, styles.normal, styles.semidark]}>{item.contact}</Text>
				      	</View>
                      
                      
                    </TouchableOpacity> 
                  )
                })
            }
	      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	    flexDirection: 'column',
	    backgroundColor: '#f7f7f7',
	    padding: 5,
	},

	header: {
		flexDirection: 'column',
	},

	content: {
		
	},

	h1: {
		fontSize: 20,
	},

	h2: {
		fontSize: 18,
	},

	h3: {
		fontSize: 16,
	},

	h1Icon: {
		fontSize: 32,
	},

	h15Icon: {
		fontSize: 25,
	},
	
	h2Icon: {
		fontSize: 18,
	},

	normal: {
		
		fontSize: 12,
	},

	font: {
		fontFamily: 'QuattrocentoSans',
	},

	fontBold: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
	},

	dark: {
		color: '#000000',
	},

	light: {
		color: '#ffffff',
	},

	semidark: {
		color: '#b7b7b7'
	},

	activeLink: {
		borderBottomWidth: 3,
		borderBottomColor: '#465bf7',
	},

	button: {
		flexDirection: 'row', 
		backgroundColor: '#465bf7', 
		justifyContent: 'center', 
		alignItems: 'center',
		padding: 5,
		paddingRight: 10,
		borderRadius: 100,
	},

	form: {
		
	},

	listItem: {
		flexDirection: 'row',
		alignItems: 'center',
		padding: 10,
	},

	listThumbNailWrapper: {
		backgroundColor: '#465bf7', 
		width: 55,
		height: 55,
		flexDirection: 'row', 
		alignItems: 'center',
		borderRadius: 35,
		justifyContent:'center'
	},

	listThumbNail: {
		width: 50,
		height: 50,
		borderRadius: 25,
	},

	listBody: {
		padding: 10,
	},


});


export default ListScreen2;