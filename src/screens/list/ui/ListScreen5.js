'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity
} from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation';

class ListScreen5 extends Component {
  	
  	constructor(props) {
	  super(props);
	  this.state = {
	  	categories: [
	  		{key: 0, title: 'React', selected: false},
	  		{key: 1, title: 'React Native', selected: false},
	  		{key: 2, title: 'Gaming', selected: false},
	  		{key: 3, title: 'Dota2', selected: false},
	  		{key: 4, title: 'UI', selected: false},
	  		{key: 5, title: 'UX', selected: false},
	  		{key: 6, title: 'Programming', selected: false},
	  		{key: 7, title: 'Expo', selected: false},
	  		{key: 8, title: 'Apple', selected: false},
	  		{key: 9, title: 'iPhone', selected: false},
	  		{key: 10, title: 'Mac', selected: false},
	  		{key: 11, title: 'Driving', selected: false},
	  		{key: 12, title: 'Swimming', selected: false},
	  		{key: 13, title: 'Scuba', selected: false},
	  		{key: 14, title: 'Table Tennis', selected: false},
	  		{key: 15, title: 'App Development', selected: false},
	  		{key: 16, title: 'Website Development', selected: false},

	  	]
	  };

	  this.navigateBack = this.navigateBack.bind(this);
	}

	navigateBack() {
		console.log('-->back')
	    this.props.navigation.dispatch(NavigationActions.back())
  	}

  	toggleCategory(key){
  		let { categories } = this.state

  		if(categories[key].selected)  			
  			categories[key].selected = false
  		else
  			categories[key].selected = true

  		this.setState({categories: categories})
  	}

  	render() {
  		const {categories} = this.state;

	    return (
	      <View style={styles.container}>
	      	<View style={styles.header}>

	      		<TouchableOpacity onPress={() => this.navigateBack()}>
		      		<Text style={[styles.fontBold, styles.h1, styles.highlight]}>Quotee</Text>
		      	</TouchableOpacity>
	      		
	      		<Text style={[{paddingTop: 20,},styles.font, styles.h2, styles.light]}>
		      			Let's find out your tastes.
	      		</Text>
	      		<Text style={[styles.font, styles.h2, styles.light]}>
	      			What you are into?
	      		</Text>

	      	</View>
	      	<ScrollView contentContainerStyle={styles.content}>
	      		{categories.map((item) => { 
	      			return (
			      		<TouchableOpacity 
		                  	key={item.key} 
		                  	style={(item.selected)?styles.categorySelected:styles.category}
		                  	onPress={() => this.toggleCategory(item.key)}>

		                  	{(item.selected)? 
		                  		(
			                  		<View style={styles.categorySelected}>
			                  			<MaterialIcons name='check' style={[styles.dark, styles.h2]} />
			                  			<Text style={[styles.font, styles.h2, styles.dark]}>{item.title}</Text>	
			                  		</View>
			                  	):
			                  	(
			                  		<View style={styles.category}>
			                  			<MaterialIcons name='add' style={[styles.light, styles.h2]} />
			                  			<Text style={[styles.font, styles.h2, styles.light]}>{item.title}</Text>	
			                  		</View>
			                  	) 
		                  	}
		                </TouchableOpacity> 
               		)
                })
            }
	      	</ScrollView>
	      </View>
	    );
	  }
	}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#1d2124',
		flex: 1,
		flexDirection: 'column',
	},

	header: {
		flexDirection: 'column',
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center',
	},

	content: {
		flex: 1,
		flexDirection: 'row',
		padding: 20,
		flexWrap: 'wrap',
		justifyContent: 'center', 
		alignItems:'flex-start',
	},

	h1: {
		fontSize: 22,
	},

	h2: {
		fontSize: 18,
	},

	font: {
		fontFamily: 'QuattrocentoSans',
	},

	fontBold: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
	},


	category: {
		backgroundColor: '#33373a',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 20,
        margin: 8,
	},

	categorySelected: {
		backgroundColor: '#eece6b',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 20,
        margin: 8,
	},

	light: {
		color: '#ffffff',
	},

	dark: {
		color: '#000000',
	},

	highlight: {
		color: '#eece6b',
	},

});


export default ListScreen5;