import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  TextInput,
  Image
} from 'react-native';


import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation';

class ListScreen3 extends Component {

	constructor(props) {
    super(props);
    
    this.state = {
        designList: [
          {
            key: 1,
            name: 'RUNNING',
            icon: 'directions-run',
            navigate: 'NOTHING',
          },
          {
            key: 2,
            name: 'BIKING',
            icon: 'motorcycle',
            navigate: 'NOTHING',
          },
          {
            key: 3,
            name: 'SWIMMING',
            icon: 'pool',
            navigate: 'NOTHING',
          },
          {
            key: 4,
            name: 'FITNESS',
            icon: 'fitness-center',
            navigate: 'NOTHING',
          },
          {
            key: 5,
            name: 'NUTRITION',
            icon: 'local-dining',
            navigate: 'NOTHING',
          },
          {
            key: 6,
            name: 'MEDICINE',
            icon: 'healing',
            navigate: 'NOTHING',
          },
          {
            key: 7,
            name: 'SEARCH',
            icon: 'search',
            navigate: 'NOTHING',
          },
          
        ]
    };

    this.navigateTo = this.navigateTo.bind(this);

  }

  navigateTo(to) {
    console.log('-->', to);
    //this.props.navigation.navigate(to)
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {

  	var designList = this.state.designList;

    return (
      <View style={styles.container}>
	      <View style={styles.header}>
	      	
	      	<View style={{flex: 1,
	      		flexDirection: 'row', 
	      		justifyContent: 'space-between', 
	      		alignItems: 'center',
	      		padding: 10,}}>
	      		<MaterialIcons name="keyboard-backspace" style={[styles.light, styles.h2Icon]} />
	      		<Text style={[styles.fontBold, styles.light, styles.h2]}>MENU</Text>
	      	</View>

	      </View>

	      <ScrollView contentContainerStyle={styles.content}>
	      	{designList.map((item) => { 
	      		  
                  return (
                    <TouchableOpacity 
                      key={item.key} 
                      style={(item.name==='SEARCH')?
                                    [styles.button, styles.activeButton]:[styles.button]}
                      onPress={() => this.navigateTo(item.navigate)}>

                      	<MaterialIcons name={item.icon} 
                      		style={(item.name==='SEARCH')?[styles.h1Icon, styles.light]:[styles.h1Icon, styles.semidark]} />
					    <Text 
					    	style={(item.name==='SEARCH')?[{marginTop: 10}, styles.font, styles.h3, styles.light]: [{marginTop: 10}, styles.font, styles.h3, styles.semidark]}>{item.name}</Text>
                      
                    </TouchableOpacity> 
                  )
                })
            }
	      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	    flexDirection: 'column',
	    backgroundColor: '#18273d',
	    padding: 5,
	},

	header: {
		flexDirection: 'row',
	},

	content: {
		flex: 1,
		flexDirection: 'row', 
		flexWrap: 'wrap',
		paddingHorizontal: 30,
		justifyContent: 'space-between', 
		alignItems: 'center',
	},

	
	h1: {
		fontSize: 20,
	},

	h2: {
		fontSize: 18,
	},

	h3: {
		fontSize: 16,
	},

	h1Icon: {
		fontSize: 48,
	},

	h2Icon: {
		fontSize: 32,
	},

	normal: {
		
		fontSize: 12,
	},

	font: {
		fontFamily: 'QuattrocentoSans',
	},

	fontBold: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
	},

	dark: {
		color: '#000000',
	},

	light: {
		color: '#ffffff',
	},

	semidark: {
		color: '#939393'
	},

	button: {
		flexDirection: 'column', 
		justifyContent: 'center', 
		alignItems: 'center',
		minWidth: 130,
		marginVertical: 10,
		padding: 20,
	},

	activeButton: {
		backgroundColor: '#131e2f',
		borderRadius: 10,
	}	
	
});


export default ListScreen3;