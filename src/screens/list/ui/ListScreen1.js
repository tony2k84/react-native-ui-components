import React from "react";
import { StyleSheet, View, Text, ScrollView, TouchableOpacity } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation'

export default class ListScreens extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
        designList: [
          {
            key: 1,
            title: 'Learn React',
            detail: 'A JavaScript library for building user interfaces',
            icon: 'album',
            navigate: 'NOTHING',
          },
          {
            key: 2,
            title: 'React Native',
            detail: 'Build native mobile apps using JavaScript and React',
            icon: 'view-week',
            navigate: 'NOTHING',
          },
          {
            key: 3,
            title: 'Expo',
            detail: 'The fastest way to build an app',
            icon: 'extension',
            navigate: 'NOTHING',
          },
          {
            key: 4,
            title: 'Native Base',
            detail: 'Essential cross-platform UI components for React Native',
            icon: 'account-balance-wallet',
            navigate: 'NOTHING',
          },
          {
            key: 5,
            title: 'Semantic UI React',
            detail: 'The official Semantic-UI-React integration',
            icon: 'view-quilt',
            navigate: 'NOTHING',
          },        
        ]
    };

    this.navigateTo = this.navigateTo.bind(this);

  }

  navigateTo(to) {
    console.log('-->', to);
    //this.props.navigation.navigate(to)
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    var designList = this.state.designList;

    return (
      <View style={styles.container}>
        
        <View style={styles.header}>
          <View style={styles.headerTitleWrapper}>
            <Text style={styles.headerTitle}>My Courses</Text>
          </View>
          <View styles={styles.headerIconWrapper}>
            <MaterialIcons name="clear-all" style={styles.headerIconRight} />
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.list}>
            {designList.map((item) => { 
                  return (
                    <TouchableOpacity 
                      key={item.key} 
                      style={styles.listItem} 
                      onPress={() => this.navigateTo(item.navigate)}>
                      
                      <View style={{width: 50, justifyContent: 'center', alignItems: 'center'}}>
                        <MaterialIcons name={item.icon} style={styles.listItemIcon} />
                      </View>
                      
                      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-start', paddingLeft: 5}}>
                        <Text style={styles.listItemTitle}>{item.title}</Text>
                        <Text style={styles.listItemDetail}>{item.detail}</Text>
                      </View>
                      
                    </TouchableOpacity> 
                  )
                })
            }         
          </View>
        </View>        
      </View>    
    )
  }
}
const styles =  StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#384053',
  }, 

  header: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },

  headerTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerTitle: {
    fontFamily: 'QuattrocentoSansBold',
    color: '#eeeeee',
    fontSize: 16,
    fontWeight: 'bold',
  },

  headerIconRight: {
    color: '#ffffff',
    fontSize: 32,
  },

  content: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 20,
    paddingVertical: 25,
    backgroundColor: '#384053',
  },

  list:{
      //backgroundColor: 'steelblue',
  },

  listItem: {
    flexDirection: 'row',
    backgroundColor: '#3e495d',
    marginBottom: 10,
    borderRadius: 10,
    padding: 10,
  },

  listItemIcon: {
    fontSize: 32,
    color: '#eeeeee',
  },

  listItemTitle: {
    fontFamily: 'QuattrocentoSans',
    color: '#eeeeee',
    fontSize: 18,
  },

  listItemDetail: {
    fontFamily: 'QuattrocentoSans',
    color: '#bbbbbb',
    fontSize: 14,
  },
})