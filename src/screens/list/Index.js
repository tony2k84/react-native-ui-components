import React from 'react';

import {
  StackNavigator,
} from 'react-navigation';

import ListScreens from './List';
import ListScreen1 from './ui/ListScreen1';
import ListScreen2 from './ui/ListScreen2';
import ListScreen3 from './ui/ListScreen3';
import ListScreen4 from './ui/ListScreen4';
import ListScreen5 from './ui/ListScreen5';

const ListNavigator = StackNavigator(
	{
  		ListScreens: { 
  			path: "/lists",
  			screen: ListScreens 
  		},
  		ListScreen1: { 
  			path: "/list1",
  			screen: ListScreen1 
  		},
      ListScreen2: { 
        path: "/list2",
        screen: ListScreen2 
      },  
      ListScreen3: { 
        path: "/list3",
        screen: ListScreen3 
      }, 
      ListScreen4: { 
        path: "/list4",
        screen: ListScreen4 
      }, 
      ListScreen5: { 
        path: "/list4",
        screen: ListScreen5 
      }, 		
	},
	{
		initialRouteName: 'ListScreens',
		headerMode: 'none',
	});

export default ListNavigator;