import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'



const theme = {
	DEFAULT: '#456990',
	PRIMARY: '#F45B69',
	SECONDARY: '#E4FDE1',
};

class LoginScreen3 extends Component {

  _register(){
    console.log('Navigating register..');
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'LoginScreen4' }));
  }

  _signIn(){
    console.log('Navigating back..');
    this.props.navigation.dispatch(NavigationActions.back());
  }

  render() {
    return (
      <View style={styles.container}>
      		<View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>  
              <View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>
                <Image style={{flex: 1, resizeMode: 'cover'}} source={{ uri: 'https://cdn.pixabay.com/photo/2017/01/18/21/34/cyprus-1990939_640.jpg' }}/>
              </View>
              <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: theme.DEFAULT, opacity: 0.96}}/>
            </View>
			<View style={styles.content}>
				
				<View style={{marginTop: 80, flexDirection: 'column', alignItems: 'center',}}>
					<Feather style={styles.logo} name='at-sign'/>
				</View>

				<View style={styles.form}>
					<View style={styles.inputWrapper}>
			      	<TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.SECONDARY}
				      placeholder='Username'
				      style={styles.input}
				      />
			    </View>
			    <View style={styles.inputWrapper}>
				    <TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.SECONDARY}
				      placeholder='Password'
				      style={styles.input}
				      />
			    </View>
				</View>
			</View>
			<View style={styles.footer}>	      	
				<TouchableOpacity style={styles.buttonPrimary} onPress={() => this._signIn()}>
					<Text style={styles.buttonText}>Sign In</Text>
				</TouchableOpacity>
				<TouchableOpacity style={styles.buttonSecondary} onPress={() => this._register()}>
					<Text style={styles.buttonText}>Sign Up</Text>
				</TouchableOpacity>	      	
			</View>
			<View style={{justifyContent: 'center', alignItems: 'center', paddingBottom: 20,}}>	      	
				<Text style={{backgroundColor: 'transparent', color: theme.SECONDARY, opacity: 0.8}}>Forgot Password?</Text>     	
			</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},
	content: {
		flex: 1,
	},
	logo: {

		fontSize: 100,
		color: theme.SECONDARY,
		backgroundColor: 'transparent',
		opacity: 0.6,
	},

	form: {
		marginTop: 150,
		padding: 20,
		flex: 1,
	},

	inputWrapper: {
		flexDirection: 'row',
	    alignItems: 'center',
	    paddingVertical: 10,
	    borderBottomColor: theme.SECONDARY,
	  	borderBottomWidth: 0.5,
	  	opacity: 0.5,
	  	marginBottom: 20,
	},

	input: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.PRIMARY,
	  	paddingVertical: 10, 		  	
	},

	buttonPrimary: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 10,
		margin: 10,
		borderRadius: 100,
		backgroundColor: theme.PRIMARY,
		borderColor: theme.PRIMARY,
		borderWidth: 1,	
	},
	buttonSecondary: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 10,
		margin: 10,
		borderRadius: 100,
		backgroundColor: 'transparent',
		borderColor: theme.SECONDARY,
		borderWidth: 1,	
	},
	buttonText: {
		fontSize: 17,
		fontFamily: 'QuattrocentoSans',
		backgroundColor: 'transparent',
		color: theme.SECONDARY,
	},
	footer: {
		padding: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},

});


export default LoginScreen3;