import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'



const theme = {
	DEFAULT: '#456990',
	PRIMARY: '#F45B69',
	SECONDARY: '#FFFFFF',
};

class LoginScreen4 extends Component {
  
  _register(){
    console.log('Navigating back..')
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <View style={styles.container}>
      		<View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>  
              <View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>
                <Image style={{flex: 1, resizeMode: 'cover'}} source={{ uri: 'https://cdn.pixabay.com/photo/2017/01/18/21/34/cyprus-1990939_640.jpg' }}/>
              </View>
              <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: theme.SECONDARY, opacity: 0.95}}/>
            </View>
            <View style={{padding: 10, backgroundColor: 'transparent'}}>
            	<TouchableOpacity onPress={() => this._register()}>
            		<Feather name='arrow-left' color={theme.DEFAULT} style={{fontSize: 30}}/>
            	</TouchableOpacity>
            </View>
			<View style={styles.content}>
				<Text style={styles.heading}>Create Account</Text>
				<View style={styles.form}>
					<View style={styles.inputWrapper}>
				      	<TextInput
					      autoCorrect={false}
					      placeholderTextColor={theme.DEFAULT}
					      placeholder='Email Address'
					      style={styles.input}
					      />
				    </View>
					<View style={styles.inputWrapper}>
				      	<TextInput
					      autoCorrect={false}
					      placeholderTextColor={theme.DEFAULT}
					      placeholder='Username'
					      style={styles.input}
					      />
				    </View>
				    <View style={styles.inputWrapper}>
					    <TextInput
					      autoCorrect={false}
					      placeholderTextColor={theme.DEFAULT}
					      placeholder='Password'
					      style={styles.input}
					      />
				    </View>

				    <View style={styles.buttonWrapper}>
					    <TouchableOpacity style={styles.buttonPrimary}>
							<Text style={styles.buttonText}>Sign Up</Text>
						</TouchableOpacity>	
					</View>

				</View>
			</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},
	content: {
		flex: 1,
	},

	heading: {
		marginTop: 80,
		fontFamily: 'QuattrocentoSans',
  		fontSize: 22,
	  	color: theme.DEFAULT,
	  	backgroundColor: 'transparent',
	  	textAlign: 'center',
	},
	
	form: {
		marginTop: 100,
		padding: 20,
		flex: 1,
	},

	inputWrapper: {
		flexDirection: 'row',
	    alignItems: 'center',
	    paddingVertical: 10,
	    borderBottomColor: theme.DEFAULT,
	  	borderBottomWidth: 1,
	  	marginBottom: 20,
	},

	input: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.DEFAULT,
	  	paddingVertical: 10, 		  	
	},

	buttonWrapper: {
		
	},

	buttonPrimary: {
		marginTop: 10,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 15,
		borderRadius: 100,
		backgroundColor: theme.PRIMARY,
		borderColor: theme.PRIMARY,
		borderWidth: 1,	
	},

	buttonText: {
		fontSize: 17,
		fontFamily: 'QuattrocentoSans',
		backgroundColor: 'transparent',
		color: theme.SECONDARY,
	},
	footer: {
		padding: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},

});


export default LoginScreen4;