'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	STANDARD: '#FFFFFF',
	DEFAULT: '#212121',
	PRIMARY: '#009bf6',
	SECONDARY: '#f2e457',
};

class ProfileScreenImages extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (    	
    	<View style={styles.container}>
	    	<View style={styles.header}>
	    		<Feather name='zap' style={styles.icon}/>
	    		<Text style={styles.pageTitle}>Profile</Text>
	    		<TouchableOpacity onPress={() => this._back()}>
	    			<Feather name='layers' style={styles.icon}/>
	    		</TouchableOpacity>
	      	</View>
	      	<View style={styles.content}>
		      	<View style={[styles.row,{backgroundColor: theme.PRIMARY,}]}>
		      		<View style={styles.button}><Text style={styles.buttonText}>Follow</Text></View>		      	
		      		<Text style={styles.buttonText}>/</Text>
		      		<View style={styles.button}><Text style={styles.buttonText}>Message</Text></View>		      	
		      	</View>
		      	<View style={{flex: 3, backgroundColor: theme.SECONDARY, justifyContent: 'center', }}>
		      		<View style={styles.row}>
		      			<View style={styles.imageWrapper}>
		      				<Image style={{flex:1, borderRadius: 60}} source={{ uri: 'https://cdn.pixabay.com/photo/2016/11/16/03/57/redhead-1828099_640.jpg' }}/>
		      				<View style={{borderRadius: 60, position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: theme.PRIMARY, opacity: 0.6}}/>
		      			</View>
	      			</View>
	      			<View style={styles.row}>
	      				<View style={styles.col}>
			      			<Text style={styles.heading}>Kate Loane</Text>
			      			<Text style={styles.subHeading}>Kuala Lumpur, Malaysia</Text>
		      			</View>
	      			</View>
	      			<View style={styles.row}>
	      				<View style={[styles.col,{paddingBottom: 2, borderBottomWidth: 2, borderBottomColor: theme.DEFAULT}]}>
	      					<Feather name='thumbs-up' style={styles.icondark}/>
	      					<Text style={styles.subHeading}>LIKES</Text>
	      				</View>
	      				<View style={styles.col}>
	      					<Feather name='bookmark' style={styles.icondark}/>
	      					<Text style={styles.subHeading}>FAVORITES</Text>
	      				</View>
	      				<View style={styles.col}>
	      					<Feather name='user' style={styles.icondark}/>
	      					<Text style={styles.subHeading}>ACCONT</Text>
	      				</View>
	      			</View>	      			
		      	</View>
		      	<View style={{flex: 2, backgroundColor: theme.STANDARD}}>
		      		<View style={{flex: 1, flexDirection: 'row'}}>
		      			<View style={{flex: 1, flexDirection: 'column'}}>
			      			<Image style={{flex:1, }} source={{ uri: 'https://cdn.pixabay.com/photo/2018/02/18/21/18/body-of-water-3163554_640.jpg' }}/>
			      			<Image style={{flex:1, }} source={{ uri: 'https://cdn.pixabay.com/photo/2018/02/13/23/41/nature-3151869_640.jpg' }}/>
		      			</View>
	      				<Image style={{flex:1, }} source={{ uri: 'https://cdn.pixabay.com/photo/2018/02/07/14/27/pension-3137209_640.jpg' }}/>
	      			</View>
		      	</View>
	      	</View>
      	</View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		backgroundColor: theme.DEFAULT,
		padding: 15,
	},

	content: {
		flex: 1,
	},

	icon: {
		fontSize: 24,
		color: theme.STANDARD,
	},

	icondark: {
		fontSize: 24,
		color: theme.DEFAULT,
		padding: 5,
	},

	pageTitle: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.STANDARD,
	},

	heading: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
  		fontSize: 22,
	  	color: theme.DEFAULT,
	},

	subHeading: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 13,
	  	color: theme.DEFAULT,
	},

	row: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		padding: 20,
	},

	col: {
		flexDirection: 'column',
		justifyContent: 'space-around',
		alignItems: 'center',
	},

	buttonText: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.STANDARD,
	},

	imageWrapper: {
		flexDirection: 'column',
		borderRadius: 80,
		width: 120,
		height: 120,
	},
});


export default ProfileScreenImages;