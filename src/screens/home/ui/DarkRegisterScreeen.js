'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#161415',
	PRIMARY: '#fefefe',
	SECONDARY: '#8c8b8b',
	HIGHLIGHT: '#6a6343',
};


class DarkRegisterScreeen extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }
  
  render() {
    return (
    	<View style={styles.container}>
	    	<View style={styles.header}>
	    		<TouchableOpacity onPress={() => this._back()}>
	    			<Feather name='chevron-left' style={styles.icon}/>
	    		</TouchableOpacity>
	    		<View/>
	    	</View>
	    	<View style={styles.content}>
	    		<Text style={styles.heading}>Ink</Text>
	    		<View style={{marginTop: 50}}/>
	    		<Text style={styles.subHeading}>To create and account or sign in, use your phone number</Text>

	    		<View style={{marginTop: 100}}/>
	    		<View style={styles.inputWrapper}>
			      	<TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.PRIMARY}
				      placeholder='Malaysia'
				      style={styles.input}
				      />
			    </View>

			    <View style={styles.inputWrapper}>
			    	<Text style={[styles.textInputLabel, {marginRight: -30}]}>+60</Text>
			      	<TextInput
				      autoCorrect={false}
				      placeholderTextColor={theme.PRIMARY}
				      placeholder='Phone number'
				      style={styles.input}
				      />
			    </View>

			    <View style={{marginTop: 20}}/>
			    <TouchableOpacity style={styles.button}>
					<Text style={styles.buttonText}>Done</Text>
				</TouchableOpacity>
	    	</View>
	    	<View style={styles.footer}>
	    		<Text style={styles.text}>
	    			By clicking Done, you agree that you have had an opportunity to review and are consenting without  reservation to the following <Text style={styles.subText}>terms and conditions</Text>
	    		</Text>
	    	</View>
    	</View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10,
	},

	column: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},

	content: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		paddingLeft: 60,
		paddingRight: 60,
	},

	heading: {
		fontFamily: 'QuattrocentoSansBold',
  		fontSize: 55,
  		fontWeight: 'bold',
	  	color: theme.PRIMARY,
	},

	subHeading: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 20,
	  	color: theme.SECONDARY,
	  	textAlign: 'center',
	},

	text: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 16,
	  	color: theme.SECONDARY,
	  	textAlign: 'center',
	},

	subText: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 16,
	  	color: theme.HIGHLIGHT,
	  	textAlign: 'center',
	},

	icon: {
		fontSize: 24,
		color: theme.PRIMARY,
	},

	inputWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingHorizontal: 15,
	    paddingVertical: 10,
	    backgroundColor: 'transparent',
	    borderBottomWidth: 0.8,
	    borderBottomColor: theme.SECONDARY,
	},

	input: {
		flex: 1,
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.PRIMARY,
	  	paddingVertical: 10,
	  	paddingHorizontal: 10,
	  	textAlign: 'center',	  	
	},

	textInputLabel: {
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.PRIMARY,
	  	paddingVertical: 10,
	  	paddingHorizontal: 10,
	  	textAlign: 'center',
	},

	button: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		paddingHorizontal: 15,
	    paddingVertical: 10,
	    backgroundColor: 'transparent',
	},

	buttonText: {
		fontFamily: 'QuattrocentoSansBold',
  		fontSize: 20,
	  	color: theme.HIGHLIGHT,
	  	paddingVertical: 10,
	  	paddingHorizontal: 10,
	  	textAlign: 'center',
	},

	footer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingLeft: 50,
		paddingRight: 50,
		paddingBottom: 20,
	},

});


export default DarkRegisterScreeen;