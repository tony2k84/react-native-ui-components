import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FDFFFC',
	PRIMARY: '#020100',
	SECONDARY: '#C5C3C6',
	HIGHLIGHT: '#134074',
};

class MobileTableConcept1 extends Component {
  
  constructor(props) {
    super(props);
  
    this.state = {
    	orders: [1234,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1245,1246]
    };
  }

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }
  
  render() {
  	const { orders } = this.state;
    return (
      <View style={styles.container}>
	      <View style={styles.header}>
	      	<TouchableOpacity onPress={() => this._back()}><Feather name='menu' style={styles.icon}/></TouchableOpacity>
	      	<Text style={styles.title}>Order Report</Text>
	      	<Feather name='search' style={styles.icon}/>
	      </View>
	      <View style={styles.header}>
			<Feather name='arrow-left' style={styles.icon}/>	      
	      	<View style={styles.item}>
	      		<Text style={styles.text}>Approved</Text>
	      		<Feather name='chevron-down' style={styles.icon}/>
	      	</View>
	      	<View style={styles.item}>
	      		<Feather name='filter' style={styles.icon}/>
	      		<Text style={styles.text}>Sort By</Text>	      		
	      	</View>
	      	<Feather name='arrow-right' style={styles.icon}/>
	      </View>
	      <View style={styles.content}>

	      	<View style={styles.thead}>
		      	<Text style={styles.th}>Order#</Text>
		      	<Text style={styles.th}>Date</Text>
		      	<Text style={styles.th}>Type</Text>
		      	<Text style={styles.th}>Status</Text>
	      	</View>
	      	<ScrollView style={styles.tbody}>
			    {orders.map((order) => {          
		              return (
		                <View style={styles.tr} key={order}>
					      	<Text style={styles.td}>{order}</Text>
					      	<Text style={styles.td}>May 2{"\n"}10:54:00</Text>
					      	<Text style={styles.td}>New Registration</Text>
					      	<Text style={styles.td}>Pending Approval</Text>
				        </View>
		              )
		            })
	          	}
	        </ScrollView>	      	
	      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.DEFAULT,
	},

	header: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 10,
		borderBottomWidth: 1.5,
		borderBottomColor: theme.SECONDARY,
	},
	content: {
		flex: 1,
		flexDirection: 'column',
		//justifyContent: 'center',
		alignItems: 'center',
	},

	icon: {
		fontSize: 20,
		padding: 5,
		color: theme.HIGHLIGHT,
	},

	title: {
		fontSize: 22,
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		color: theme.PRIMARY,
	},

	item: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		color: theme.PRIMARY,
	},

	text: {
		fontSize: 16,
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		color: theme.PRIMARY,
	},

	thead: {
		flexDirection: 'row',
		paddingLeft: 10,
		paddingRight: 10,
		paddingBottom: 10,
		paddingTop: 10,
		backgroundColor: theme.HIGHLIGHT,		
	},

	th: {
		width: '25%',
		fontSize: 18,
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		color: theme.DEFAULT,
	},

	tbody: {
		flexDirection: 'column',
		padding: 10,		
	},

	tr: {
		flexDirection: 'row',
	},

	td: {
		width: '25%',
		//textAlign: 'center',
		fontSize: 15,
		fontFamily: 'QuattrocentoSans',
		color: theme.PRIMARY,
		paddingBottom: 10,
	},
});


export default MobileTableConcept1;