import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';

import {
  StackNavigator,
  NavigationActions
} from 'react-navigation';

import Feather from 'react-native-vector-icons/Feather';

export default class LoginScreens extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
        designList: [
          {
            key: 2,
            title: 'Dark LoginUI #1',
            detail: 'A ligh dark elegant UI',
            navigate: 'DarkLoginScreen2',
          },        
          {
            key: 3,
            title: 'Clean White And Blue Login UI',
            detail: 'Simple yet appealing',
            navigate: 'LoginScreen3',
          }, 
          {
            key: 4,
            title: 'Minimal Login Journey',
            detail: 'Minimal Login Journey',
            navigate: 'LoginScreen5',
          }, 
          {
            key: 5,
            title: 'Edge to Edge Login',
            detail: 'Edge to Edge Login',
            navigate: 'LoginScreen6',
          },
          {
            key: 6,
            title: 'Shopping App Login',
            detail: 'Bordered UI Simple',
            navigate: 'LoginScreen7',
          }, 
          {
            key: 7,
            title: 'Border Less Login Form',
            detail: 'Border Less Login Form',
            navigate: 'LoginScreen8',
          }, 
        ]
    };

    this.navigateTo = this.navigateTo.bind(this);
    this.back = this.back.bind(this);

  }

  navigateTo(to) {
    this.props.navigation.navigate(to)
  }

  back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    var designList = this.state.designList;

    return (
      <View style={styles.container}>
          <StatusBar hidden/>
          <View style={styles.header}>
            
            <TouchableOpacity onPress={this.back}>
              <Feather name='chevron-left' style={styles.icon}/>
            </TouchableOpacity>

            <Text style={styles.listTitle}>Login</Text>

          </View>
          
          <View style={styles.list}>
          {designList.map((item) => {          
              return (
                <TouchableOpacity style={styles.listItem} key={item.key} onPress={() => this.navigateTo(item.navigate)}>
                  <Text style={styles.itemText}>{item.title}</Text>
                  <Text style={styles.itemSubText}>{item.detail}</Text>
                </TouchableOpacity>                  
              )
            })
          }
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFFFFF',
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 10,
    marginBottom: 30,
  },

  icon: {
    fontSize: 40,
    color: '#6A6262',
  },

  list: {
    padding: 10,
  },

  listTitle: {
    fontFamily: 'QuattrocentoSansBold',
    fontWeight: 'bold',
    fontSize: 28,
    color: '#6A6262',
  },

  listItem: {
    paddingVertical: 20,
  },

  itemText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 20,
    color: '#6A6262',   
  },

  itemSubText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 16,
    color: '#9C9990', 
  }
});