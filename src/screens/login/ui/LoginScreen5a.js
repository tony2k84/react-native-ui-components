//https://in.pinterest.com/pin/292452569544127646/

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#313E50',
	SECONDARY: '#2967cc',
	DISABLED: '#878C8F',
};

class LoginScreen5a extends Component {
  
  _next(){
    this.props.navigation.dispatch(NavigationActions.navigate({ routeName: 'LoginScreen5b' }));
  }

  _back(){
    this.props.navigation.dispatch(NavigationActions.back());
  }

  render() {
    return (
      <View style={styles.container}>
      	<View style={styles.content}>
      		<Text style={styles.heading}>
      			Hi,
      		</Text>

      		<View style={{marginTop: 100}}/>
      		<View style={styles.inputWrapper}>
			    <TextInput
			      autoCorrect={false}
			      placeholderTextColor={theme.PRIMARY}
			      placeholder='Your email address..'
			      style={styles.input}
			      />
		    </View>		
      	</View>	
      	<View style={styles.footer}>
      		<TouchableOpacity onPress={() => this._back()}>
      			<Feather name='arrow-left' style={styles.icon}/>
      		</TouchableOpacity>

      		<TouchableOpacity onPress={() => this._next()}>
      			<Text style={styles.linkText}>Next</Text>
      		</TouchableOpacity>
      	</View>	
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
	},

	content: {
		flex: 1,
	},

	heading: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
  		fontSize: 36,
	  	color: theme.PRIMARY,
	  	marginTop: 30,
	  	marginBottom: 10,
	},

	subText: {
		fontFamily: 'QuattrocentoSans',
		fontSize: 17,
	  	color: theme.DISABLED,
	},

	linkText: {
		fontFamily: 'QuattrocentoSansBold',
		fontWeight: 'bold',
		fontSize: 19,
	  	color: theme.SECONDARY,
	},

	inputWrapper: {
		flexDirection: 'row',
	    alignItems: 'center',
	    paddingVertical: 10,	    
	},

	input: {
		flex: 1,
		fontFamily: 'QuattrocentoSans',
  		fontSize: 20,
	  	color: theme.PRIMARY,
	  	paddingVertical: 10, 		  	
	},

	icon: {
		fontSize: 24,
		color: theme.SECONDARY,
	},

	footer: {
		paddingVertical: 10,
		flexDirection: 'row',
		justifyContent: 'space-between',
	}
});


export default LoginScreen5a;