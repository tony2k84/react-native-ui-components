import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView
} from 'react-native';

import {
  StackNavigator,
  NavigationActions
} from 'react-navigation';

import Feather from 'react-native-vector-icons/Feather';

export default class LoginScreens extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
        designList: [
          {
            key: 1,
            title: 'Clean Contact US Page',
            detail: 'Clean Contact US Page',
            navigate: 'ContactUsScreen1',
          },
          {
            key: 2,
            title: 'Stylish Profile Screen',
            detail: 'Stylish Profile Screen',
            navigate: 'ProfileScreenImages',
          },
          {
            key: 3,
            title: 'Today View',
            detail: 'Compact and to the point',
            navigate: 'TodayView',
          },
          {
            key: 4,
            title: 'Task Summary Minimal',
            detail: 'Task Summary Minimal',
            navigate: 'TaskSummaryView',
          },
          {
            key: 5,
            title: 'Hotel Promotion View',
            detail: 'Minimal Hotel Promotion Full View',
            navigate: 'HotelPromotion',
          },
          {
            key: 6,
            title: 'DarkRegisterScreen',
            detail: 'Register with Mobile',
            navigate: 'DarkRegisterScreeen',
          },
          {
            key: 7,
            title: 'Mobile Table Concept',
            detail: 'Mobile Table Concept',
            navigate: 'MobileTableConcept1',
          },
          {
            key: 7,
            title: 'Data Usage Tracking',
            detail: 'Data Usage Tracking',
            navigate: 'DataUsageTracking1',
          },
        ]
    };

    this.navigateTo = this.navigateTo.bind(this);
    this.back = this.back.bind(this);

  }

  navigateTo(to) {
    this.props.navigation.navigate(to)
  }

  back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    var designList = this.state.designList;

    return (
      <View style={styles.container}>
          <StatusBar hidden/>
          <View style={styles.header}>
            
            <TouchableOpacity onPress={this.back}>
              <Feather name='chevron-left' style={styles.icon}/>
            </TouchableOpacity>

            <Text style={styles.listTitle}>General</Text>

          </View>
          
          <ScrollView style={styles.list}>
          {designList.map((item) => {          
              return (
                <TouchableOpacity style={styles.listItem} key={item.key} onPress={() => this.navigateTo(item.navigate)}>
                  <Text style={styles.itemText}>{item.title}</Text>
                  <Text style={styles.itemSubText}>{item.detail}</Text>
                </TouchableOpacity>                  
              )
            })
          }
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFFFFF',
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 10,
    marginBottom: 30,
  },

  icon: {
    fontSize: 40,
    color: '#6A6262',
  },

  list: {
    padding: 10,
  },

  listTitle: {
    fontFamily: 'QuattrocentoSansBold',
    fontWeight: 'bold',
    fontSize: 28,
    color: '#6A6262',
  },

  listItem: {
    paddingVertical: 20,
  },

  itemText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 20,
    color: '#6A6262',   
  },

  itemSubText: {
    fontFamily: 'QuattrocentoSans',
    fontSize: 16,
    color: '#9C9990', 
  }
});