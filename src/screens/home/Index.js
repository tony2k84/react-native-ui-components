import React from 'react';

import {
  StackNavigator,
} from 'react-navigation';

import HomeScreens from './Home';
import ContactUsScreen1 from './ui/ContactUsScreen1';
import ProfileScreenImages from './ui/ProfileScreenImages';
import TodayView from './ui/TodayView';
import TaskSummaryView from './ui/TaskSummaryView';
import HotelPromotion from './ui/HotelPromotion';
import DarkRegisterScreeen from './ui/DarkRegisterScreeen';
import MobileTableConcept1 from './ui/MobileTableConcept1';
import DataUsageTracking1 from './ui/DataUsageTracking1';

const HomeNavigator = StackNavigator(
	{
  		HomeScreens: { 
  			path: "/home",
  			screen: HomeScreens 
  		},
  		ContactUsScreen1: { 
  			path: "/screen1",
  			screen: ContactUsScreen1 
  		},
      ProfileScreenImages: { 
        path: "/screen2",
        screen: ProfileScreenImages 
      },
      TodayView: { 
        path: "/screen3",
        screen: TodayView 
      },
      TaskSummaryView: { 
        path: "/screen4",
        screen: TaskSummaryView 
      },
      HotelPromotion: { 
        path: "/screen5",
        screen: HotelPromotion 
      },
      DarkRegisterScreeen: {
        path: "/screen6",
        screen: DarkRegisterScreeen
      },  		
      MobileTableConcept1: {
        path: "/screen7",
        screen: MobileTableConcept1
      },
      DataUsageTracking1: {
        path: "/screen8",
        screen: DataUsageTracking1
      }
	},
	{
		initialRouteName: 'HomeScreens',
		headerMode: 'none',
	});

export default HomeNavigator;