'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#3bb1da',
};

class HotelPromotion extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <View style={styles.container}>
      		<View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>  
              <View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>
                <Image style={{flex: 1, resizeMode: 'cover'}} source={{ uri: 'https://cdn.pixabay.com/photo/2018/04/20/00/06/sunset-3334725_640.jpg' }}/>
              </View>
              <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: theme.DEFAULT, opacity: 0.3}}/>
            </View>
            <View style={styles.content}></View>
			<View style={styles.footer}>
				<View style={styles.row}>
					<TouchableOpacity style={styles.button} onPress={() => this._back()}>
						<MaterialIcons name='flight' style={styles.icon}/>
						<View style={{paddingLeft: 5}}/>
						<Text style={styles.buttonText}>
							INCLUDED
						</Text>
					</TouchableOpacity>
				</View>
				<View style={{paddingTop: 20}}/>
				<Text style={styles.heading}>
					Riverand Alpline Groups Hotel and Spa
				</Text>

				<View style={styles.row}>
					<Feather name='map-pin' style={styles.icon}/>
					<View style={{paddingLeft: 10}}/>
					<Text style={styles.subHeading}>
						Grand Satan, Switzerland
					</Text>
				</View>


				<View style={{paddingTop: 50}}/>

				<View style={styles.rowspaced}>
					<View style={{flexDirection: 'row'}}>
						<Text style={styles.subText}>
							From <Text style={{color: theme.PRIMARY}}>$1,299</Text> for 7 nights
						</Text>
					</View>
					<View style={styles.rating}>
						<Text style={styles.subText}>6.02</Text>
					</View>
				</View>
				
			</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},

	content: {
		flex: 1,
	},

	footer: {
		padding: 20,
		flexDirection: 'column',
		backgroundColor: 'transparent',
	},

	row: {
		flexDirection: 'row',
		paddingTop: 10,
	},

	rowspaced: {
		flexDirection: 'row',
		paddingTop: 10,
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	icon: {
		fontSize: 20,
		color: theme.DEFAULT,
	},

	button: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: theme.PRIMARY,
		padding: 10,
		borderRadius: 20,
	},

	heading: {
		fontSize: 28,
		color: theme.DEFAULT,
		backgroundColor: 'transparent',
		fontFamily: 'QuattrocentoSansBold',
	},

	subHeading: {
		fontSize: 16,
		color: theme.DEFAULT,
		backgroundColor: 'transparent',
		fontFamily: 'QuattrocentoSans',
	},

	buttonText: {
		fontSize: 14,
		color: theme.DEFAULT,
		backgroundColor: 'transparent',
		fontFamily: 'QuattrocentoSansBold',
	},

	subText: {
		fontSize: 14,
		color: theme.DEFAULT,
		backgroundColor: 'transparent',
		fontFamily: 'QuattrocentoSans',
	},

	rating: {
		width: 40,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		borderWidth: 1.5,
		borderRadius: 100,
		borderColor: theme.DEFAULT,
	}

});


export default HotelPromotion;