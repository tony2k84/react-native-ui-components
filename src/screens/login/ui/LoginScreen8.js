import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

import { LinearGradient } from 'expo';


import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#FFFFFF',
	PRIMARY: '#C39DB4',
};

class LoginScreen8 extends Component {

  _back(){
    this.props.navigation.dispatch(NavigationActions.back())
  }

  render() {
    return (
      <View style={styles.container}>
      	<LinearGradient
      	  colors={['#914D76', '#453F3C']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
          }}
        />

      	<View style={styles.header}>
      		<View style={styles.dotSelected}><View style={styles.dot}></View></View>
      		<View style={styles.line}></View>
      		<View style={styles.dot}></View>
      		<View style={styles.line}></View>
      		<View style={styles.dot}></View>
      	</View>
      	<View style={styles.content}>
      		<Text style={styles.heading}>CREATE ACCOUNT</Text>

      		<View style={{paddingTop: 100,}}/>
      		<View style={styles.inputWrapper}>
      			<View style={{backgroundColor: theme.PRIMARY,borderRadius: 100, opacity: 0.2, position: 'absolute',top:0,left: 0,width: '100%',height: '100%'}}>
      			</View>

      			<View style={{paddingLeft: 25,}}/>
      			
				<Feather name='align-left' style={styles.icon}/>
		      	<TextInput
			      autoCorrect={false}
			      placeholderTextColor={theme.DEFAULT}
			      placeholder='Name'
			      style={styles.input}
			      />
		    </View>
		    <View style={{paddingTop: 20,}}/>
      		<View style={styles.inputWrapper}>
      			<View style={{backgroundColor: theme.PRIMARY,borderRadius: 100, opacity: 0.2, position: 'absolute',top:0,left: 0,width: '100%',height: '100%'}}>
      			</View>

      			<View style={{paddingLeft: 25,}}/>
      			
				<Feather name='mail' style={styles.icon}/>
		      	<TextInput
			      autoCorrect={false}
			      placeholderTextColor={theme.DEFAULT}
			      placeholder='Email'
			      style={styles.input}
			      />
		    </View>
		    <View style={{paddingTop: 20,}}/>
      		<View style={styles.inputWrapper}>
      			<View style={{backgroundColor: theme.PRIMARY,borderRadius: 100, opacity: 0.2, position: 'absolute',top:0,left: 0,width: '100%',height: '100%'}}>
      			</View>

      			<View style={{paddingLeft: 25,}}/>
      			
				<Feather name='lock' style={styles.icon}/>
		      	<TextInput
			      autoCorrect={false}
			      placeholderTextColor={theme.DEFAULT}
			      placeholder='Password'
			      style={styles.input}
			      />
		    </View>

		    <View style={{paddingTop: 40,}}/>
		    <TouchableOpacity style={styles.button} onPress={() => this._back()}>
		    	<Text style={styles.buttonText}>CONTINUE</Text>
		    </TouchableOpacity>

      	</View>
      	<View style={styles.footer}>
      		<Text style={styles.subText}>Terms & Conditions</Text>
      	</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
	},

	header: {
		height: 80,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},

	dot: {
		width: 5,
		height: 5,
		borderRadius: 5,
		backgroundColor: theme.DEFAULT,
		margin: 10,
	},

	dotSelected: {
		borderWidth: 0.2,
		borderColor: theme.DEFAULT,
		borderRadius: 50,
		padding: 3,
		margin: 10,
	},

	line: {
		flex: 1,
		height: 1,
		borderBottomWidth: 0.2,
		borderBottomColor: theme.DEFAULT,
	},

	heading: {
		color: theme.DEFAULT,
		fontSize: 24,
		fontFamily: 'QuattrocentoSansBold',
		backgroundColor: 'transparent',
	},

	subText: {
		color: theme.PRIMARY,
		fontSize: 14,
		fontFamily: 'QuattrocentoSans',
		backgroundColor: 'transparent',
	},

	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},

	inputWrapper: {
		flexDirection: 'row',
		alignItems: 'center',
	},

	icon: {
		fontSize: 24,
		color: theme.PRIMARY,
		backgroundColor: 'transparent',
	},

	input: {
		flex: 1,
		fontFamily: 'QuattrocentoSans',
  		fontSize: 17,
	  	color: theme.DEFAULT,
	  	paddingVertical: 15,
	  	paddingHorizontal: 15,	  	
	},

	button: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	    paddingVertical: 15,
	    backgroundColor: 'transparent',
	    borderRadius: 100,
	    borderWidth: 0.5,
	    borderColor: theme.PRIMARY,
	},

	buttonText: {
		flex: 1,
		fontSize: 16,
		fontFamily: 'QuattrocentoSans',
		color: theme.DEFAULT,
		textAlign: 'center',
	},

	footer: {
		backgroundColor: 'transparent',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',		
	},
});


export default LoginScreen8;