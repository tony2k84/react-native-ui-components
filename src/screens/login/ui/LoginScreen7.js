import React, { Component, } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  TextInput
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import { NavigationActions } from 'react-navigation'

const theme = {
	DEFAULT: '#313131',
	PRIMARY: '#FEFEFE',
};

class LoginScreen7 extends Component {
  
  _back(){
    this.props.navigation.dispatch(NavigationActions.back());
  }

  render() {
    return (
      	<View style={styles.container}>
      		<View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>  
              <View style={{position: 'absolute',top: 0,left: 0, width: '100%',height: '100%',}}>
                <Image style={{flex: 1, resizeMode: 'cover'}} source={{ uri: 'https://cdn.pixabay.com/photo/2016/11/23/17/08/bag-1853847_640.jpg' }}/>
              </View>
              <View style={{position: 'absolute',top: 0,left: 0,width: '100%',height: '100%',backgroundColor: theme.DEFAULT, opacity: 0.7}}/>
            </View>
            <View style={styles.content}>
            	<Feather name='shopping-cart' style={styles.logo}/>
            	<View style={{paddingBottom: 50}}/>
            	<Text style={styles.title}>Have a fun shopping experience with us.</Text>
			</View>
			<View style={styles.footer}>
				<View style={{flexDirection: 'row', backgroundColor: 'transparent', padding: 10,}}>
					<TouchableOpacity style={styles.button} onPress={() => this._back()}>
						<Text style={styles.buttonText}>LOGIN</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.button} onPress={() => this._back()}>
						<Text style={styles.buttonText}>SIGN UP</Text>
					</TouchableOpacity>
				</View>
				<View style={{flexDirection: 'row', backgroundColor: 'transparent', padding: 10,}}>
					<TouchableOpacity style={styles.button} onPress={() => this._back()}>
						<Feather name='facebook' style={styles.icon}/>
						<View style={{paddingRight: 10}}/>
						<Text style={styles.buttonText}>CONNECT WITH FACEBOOK</Text>
					</TouchableOpacity>
				</View>
				<View style={{justifyContent: 'center', alignItems: 'center', padding: 20}}>				
					<Text style={styles.smallText}>Terms and Conditions</Text>
				</View>
			</View>
      	</View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		flex: 1,
		backgroundColor: 'transparent',
		justifyContent: 'center',
		alignItems: 'center',
	},

	footer: {
		backgroundColor: 'transparent',
		flexDirection: 'column',
	},

	logo: {
		color: theme.PRIMARY,
		fontSize: 100,
	},

	title: {
		color: theme.PRIMARY,
		fontSize: 28,
		fontFamily: 'QuattrocentoSansBold',
		textAlign: 'center',
	},

	icon: {
		color: theme.PRIMARY,
		fontSize: 20,
	},

	button: {
		flex: 1,
		marginHorizontal: 10,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
	    paddingVertical: 20,
	    backgroundColor: 'transparent',
	    borderWidth: 1,
	    borderColor: theme.PRIMARY,
	},

	buttonText: {
		fontSize: 16,
		fontFamily: 'QuattrocentoSans',
		color: theme.PRIMARY,
	},

	smallText: {
		fontSize: 14,
		fontFamily: 'QuattrocentoSans',
		color: theme.PRIMARY,	
	}


});


export default LoginScreen7;